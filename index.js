// JS comments
// They are important , acts as a guid for the programmer.
//  Comment are disregarded no matter how long it may be
	// Multi-Line Coments ctrl=shift+/
	/* This 
	is
	a
	multi
	line
	comment*/

// STATEMENTS
// Programming instruction that we tell the computer to perform
// Statments usually ends with semicolon (;)

// SYNTAX
// It is the set of rules that describes how statements must be constructed.

// alert("Hello Again");

// This is a statement with a correct syntax

console.log("Hello World");

// JS is loose type Programming Language
console. log (" Hello	World   "   );

// Also with this
console.
log
(
"Hello Again"
)

//[SECTION] Variables
// Is is used as a  container or storage

// Declaring variable - tell our device that a variable name is create and is ready to store data

// Syntax -  let/const variableName;

// "let" is a keyword that is usually used to declare a vairable.

let	 myVariable ;
let	hello;
console.log(myVariable);
console.log(hello);

// "=" sign stands for initialization, that mean giving a value to a variable

// Guidline for declaring a variable

/*
	1. use let keyword followed by a variable name and then followed again by the assignment =.
	2. variable names should start with a a lowercase letter.
	3. for constant variable we are using const keyword.
	4. variable names should be comprehemsive or descriptive.
	5. do not use spaces on declared variable
	6. all vairable name should be unique and cannot be same name
*/

// DIFFERENT CASING STYLES
// cammel case - thisIsCammelCasing
// snake case - this_is_snake_casing
// kebab case - this-is-kebab-casing

let firstName = "Michael"; // this is a good variable name

let pokemon = 25000;	// this is a bad variable name

//Declaring and initializing variables
// Syntax -> let/const vairableName = value;

let productName = "Desktop Computer";

console.log(productName);

let	productPrice = 18999;

console.log(productPrice);

// re assigning a value to a variable.

productPrice = 25000;

console.log(productPrice);

let friend = "kate";
friend = "jane";
console.log(friend);

let supplier;
supplier = "John Smith Tradings";
console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier);

// let vs const
// let variables values can be changed, we can re-assign new value.
// const variables value cannot be changed

const hoursInAday = 24;
console.log(hoursInAday);

const pie = 3.14;
console.log(pie);

// local/global scope variable
let outerVariable = "Hi";

{
	let innerVariable = "Hello Again"; // This is a local variable // can only be accessed locally or inside the {}
	console.log(innerVariable);
	console.log(outerVariable);

}

console.log(outerVariable)

// MULTIPLE VARIABLE DECLARTION
// Multiple variables may be declared in one line.
// Convinient and it is easir to read the code.

// let productCode = "DC017";
// let productBrand = "Dell";

let productCode = "DC017", productBrand = "Dell";
console.log(productCode,productBrand);

// let is a reserved keyword, we will be having an error
// const let = "Hello";
// console.log(let)

// [SECTION] Dta Types

// String - are series of charactes that creates a word.

let country = "Philippines";
let province = "Metro Manila";

// Concatenating String, using + symbold
// Combining string values

let fullAddress = province +", " + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

//escape character (\)
// "\n" refers to creating a new line between text

let mailAddress = "Metro Manila\n\n Philippines";
console.log(mailAddress);

let message = "John`s employees went home early";
console.log(message);
message = 'John\'s employees went home early';
console.log(message);

let	headcount = 26;
console.log(headcount);

//Decimal or Fractions
let grade = 98.7;
console.log(grade);

// Exponential Notation "e"
let planetDistance = 2e10;
console.log(planetDistance);

//Combining text and String
console.log("John's grade last quarter is " + 98.7);

// Boolean
// we have 2 boolean values, tru and false

let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct" + isGoodConduct);

// Arrays
// Are special kind of data type
// used to store multiple values with the same data
// Sytax -> let/const arrayName = [elementA,elementB, ....]

let grades = [ 98.7, 92.1, 90.2];
console.log(grades);

let details = ["John", "Smith", 32, true];
console.log	(details);

// Objects
// Hold properties that describes the variables
// Syntax -> let/const objectName = {propertyA: vlue, propertyB: value};

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	contact: ["0123456789","9876543210"],
	address: {
		houseNumber: "345",
		city: "Manila"
	}
}

console.log(person);

let myGrades = {
	firstGrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6
}

console.log(myGrades);

// Checking of data type
console.log(typeof grades);
console.log(typeof person);
console.log(typeof isMarried);

const anime = ["one piece", "one punch man", "attack on titan"];
// anime = ["kimitsu no yaiba"];
anime[0] = "kimitsu no yaiba";
console.log(anime);

//Null vs Undefined
// Null has not value
//Undefined when a variable has no value upon declaration

let spouse = null;






